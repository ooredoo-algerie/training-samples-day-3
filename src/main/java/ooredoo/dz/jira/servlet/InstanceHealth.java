package ooredoo.dz.jira.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

@Scanned
public class InstanceHealth extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(InstanceHealth.class);

    @ComponentImport
	private final UserManager userManager;
	@ComponentImport
	private final LoginUriProvider loginUriProvider;
	@ComponentImport
	private final TemplateRenderer templateRenderer;
	@ComponentImport
	private final ProjectManager projectManager;
	
	I18nHelper i18nHelper = ComponentAccessor.getComponentOfType(I18nHelper.class);
	
    public InstanceHealth(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer, ProjectManager projectManager) {
    	this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.templateRenderer = templateRenderer;
		this.projectManager = projectManager;
    }
    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	Map<String, Object> params = new HashMap<String, Object>();
	
    	ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
    	ProjectManager projectManager = ComponentAccessor.getComponent(ProjectManager.class);
    	
		// Check if current user is authenticated
		if (ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser() == null) {
			// DO SOME ACTION
			return;
		}
		
		params.put("currentUser", currentUser.getName());
	    
		List <Project> projects = projectManager.getProjectsLeadBy(currentUser);
		params.put("projectList", projects);
		
		params.put("projectssize", projectManager.getProjects().size());
		
				
		resp.setContentType("text/html;charset=utf-8");
        resp.setContentType("text/html");
        templateRenderer.render("/templates/instanceHealthCheck.vm", params, resp.getWriter());
    }

}