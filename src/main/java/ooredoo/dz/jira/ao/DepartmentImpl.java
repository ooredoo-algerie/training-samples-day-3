package ooredoo.dz.jira.ao;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import net.java.ao.DBParam;

import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
public abstract class DepartmentImpl implements DepartmentService {

	@ComponentImport
	private static ActiveObjects ao;

	public DepartmentImpl(ActiveObjects ao) {
		this.ao = checkNotNull(ao);
	}
	
	@Override
	public Department getDepartmentByName(String Name) {
		Department[] departments = ao.find(Department.class, "DEPARTMENT_NAME = ? ", new Object[] { Name });
		if (departments.length > 0) {
			return departments[0];

		} else {
			return null;
		}
	}

	@Override
	public Department createDepartment(String Name) {
		if(Name != null) {
			Department dep = ao.create(Department.class, new DBParam("DEPARTMENT_NAME", Name));
			dep.save();
			return dep;
		}
		return null;
	}

}
