package ooredoo.dz.jira.ao;

import net.java.ao.Entity;

public interface Department extends Entity {
	public String getDepartmentName();
	public String getDepartment(String Name);
	public String setDepartment(String Name);
}
