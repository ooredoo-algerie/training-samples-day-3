package ooredoo.dz.jira.ao;

public interface DepartmentService {
	public Department getDepartmentByName(String name);
	public Department createDepartment(String name);
}
