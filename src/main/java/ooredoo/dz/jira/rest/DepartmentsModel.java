package ooredoo.dz.jira.rest;

import javax.xml.bind.annotation.*;



@XmlRootElement(name = "department")
@XmlAccessorType(XmlAccessType.FIELD)
public class DepartmentsModel {

	@XmlElement(name = "key")
	private String key;
	
    @XmlElement(name = "name")
    private String departmentName;

    public DepartmentsModel() {
    }

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

}