package ooredoo.dz.jira.rest;

import java.io.Serializable;

public class DepartmentJSON implements Serializable {

	public String key;
	public String name;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
