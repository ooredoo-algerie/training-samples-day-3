package ooredoo.dz.jira.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


/**
 * A resource of message.
 */

@Path("/department")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class departments {

  
    @GET
	@Consumes({ MediaType.APPLICATION_JSON })
    @Path ("/{key}")
    public Response getDepartment(@PathParam ("key") final String key){
    
    	DepartmentsModel department = new DepartmentsModel();
    	
    	if(key.equals("TUN"))
			department.setDepartmentName("TUNISIA");
    	else
    		department.setDepartmentName("ALGERIA");
    	
    	return Response.ok(department).build();
    }
    

    @POST
    public Response createDepartment(DepartmentJSON newDep){
    	
    	DepartmentsModel department = new DepartmentsModel();
    	department.setKey(newDep.getName());
    	department.setDepartmentName(newDep.getName());
    	
    	return Response.ok(department).build();
    }

    

    @PUT
    @Path ("/{key}")
    public Response updateDepartment(@PathParam ("key") final String key , DepartmentJSON newDep){
    	
    	DepartmentsModel department = new DepartmentsModel();
    	department.setKey(newDep.getName());
    	department.setDepartmentName(newDep.getName());
    	
    	return Response.ok(department).build();
    }



    @DELETE
    @Path ("/{key}")
    public Response delete(@PathParam ("key") final String key){
    	return Response.ok("TUNIS").build();
    }
    
}